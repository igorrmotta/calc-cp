import React, { Component } from 'react';
import './App.css';
import firebase from './firebase.js';
var Modal = require('boron/DropModal');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      userDeleteCandidate: null
    }
  }

  deleteCandidateUser(){
    var user = this.state.userDeleteCandidate;
    firebase.database().ref('users/' + user.key).set(null);
    this.setState({userDeleteCandidate: null});
    this.hideModal();
  }

  showModal(user) {
    this.setState({userDeleteCandidate: user});
    this.refs.modal.show();
  }

  hideModal() {
    this.setState({userDeleteCandidate: null});
    this.refs.modal.hide();
  }

  onModalKeyboard(event) {
    console.log(event);
  }

  filterUpdate(){
    var value = this.refs.filterInput.value;

      if (!value) {
            this.loadUsersFromFirebae();
            return;
        }
        
        var copyList = this.state.users;
        var newList = [];
        copyList.forEach(function(item, i) {
            if (item.name.indexOf(value) !== -1 || item.email.indexOf(value) !== -1) {
                newList.push(item);
            }
        });
        this.setState({users: newList});
  }

  loadUsersFromFirebae(){
 var _this = this;
    const dbRefUsers = firebase.database().ref().child('users');
    dbRefUsers.on('value', (snap) => {
      var obj = snap.val();
      var arr = Object.keys(obj).map( (key) => {
        var item = obj[key]; 
        item.key = key; 
        return item;
      });
      _this.setState({ users: arr });
    });
  }

  componentWillMount() {
   this.loadUsersFromFirebae();
  }

  renderUsers() {
    var usersList = this.state.users;
    return usersList.map((user) => {
      return (<li className="collection-item" key={user.email}>
        <span>{user.name + ', '} <span style={{ fontWeight: '800', color: '#385392' }}>{user.email}</span></span>
        <svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"
          onClick={() => this.showModal(user) }>
          <path d="M0 0h24v24H0z" fill="none"/>
          <path className="icon-remove" d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm5 11H7v-2h10v2z"/>
        </svg>
      </li>)
    });
  }

  render() {
    var modalStyle = {
      borderRadius: '2px',
      backgroundColor: '#e7eaf0',
    };

    return (
      <div className="page-container">
        <nav>
          <div className="nav-logo">
            <img src={require('./assets/lighthouse-super.svg') } alt="lighthouse super logo"></img>
          </div>
          <div className="nav-button">
            <h3>Users</h3>
          </div>
        </nav>
        <main>
          <div>
            <div style={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row',}}>
            <h1 style={{ color: '#385392' }}>Subscribed Users</h1>
             <input
                  className="search-bar"
                        type='text'
                        ref='filterInput'
                        placeholder='Type to filter..'
                        onChange={() => this.filterUpdate()}/>
                        </div>
            <ul className="collection">
              {this.renderUsers() }
            </ul>
          </div>
        </main>
        <Modal ref="modal" keyboard={this.callback} modalStyle={modalStyle}>
        <div className="modal-content">
          <h2 style={{ fontWeight: '800', color: '#536475' }}>Are you sure you want to delete this user?</h2>
          <h4 style={{color: '#536475'}}>You can't undo this action.</h4>
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around'
          }}>
            <h2 className="modal-text-button-delete" onClick={() => this.deleteCandidateUser() }>Yes</h2>
            <h2 className="modal-text-button" onClick={() => this.hideModal() }>No</h2>
          </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default App;
